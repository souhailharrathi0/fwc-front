//format date time from yyyy-mm-dd hh:mm:ss to dd/mm/yyyy hh:mm
export function formatDateTime(dateTimeString) {
  const dateTime = new Date(dateTimeString);
  const day = dateTime.getDate();
  const month = dateTime.getMonth() + 1;
  const year = dateTime.getFullYear();
  const hours = dateTime.getHours();
  const minutes = dateTime.getMinutes();
  return day + "/" + month + "/" + year + " à " + hours + ":" + minutes;
}
