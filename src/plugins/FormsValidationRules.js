const regexPhoneNumber = /^\+?[1-9]\d{1,14}$/;
const regexEmail = /.+@.+\..+/;

export default {
  requiredRules: (v) => !!v || "This field is required",

  emailRules: (v) => {
    if (v) {
      if (regexEmail.test(v)) {
        return true;
      } else {
        return "This email address is not valid!";
      }
    }
    return true;
  },

  phoneNumberRule: (v) => {
    if (v) {
      if (v.replace(/\s/g, "").match(regexPhoneNumber)) {
        return true;
      } else {
        return "This phone number is not valid!";
      }
    }
    return true;
  },

};
