import {dangerNotify} from "src/plugins/Notify";

export function responseErrorsHandler(e) {
  if (e.response && e.response.data) {
    for (const [key, value] of Object.entries(e.response.data)) {
      if (key === 'detail') {
        dangerNotify(value);
      } else {
        dangerNotify(`${key} : ${value}`);
      }
    }
  }
}
