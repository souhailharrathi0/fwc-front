import {Notify} from "quasar";

import {Dialog} from '@capacitor/dialog';

export async function showMobileConfirmation(msg) {
  const {value} = await Dialog.confirm({
    title: "Confirmation",
    message: msg,
  });
  return value;
}

//dangerNotify is a function that takes a string and displays it as a notification
Notify.registerType('danger-notif', {
  icon: 'announcement',
  progress: true,
  color: 'red',
  textColor: 'white',
  timeout: 5000,
});

//successNotify is a function that takes a string and displays it as a notification
Notify.registerType('success-notif', {
  icon: 'done',
  progress: true,
  color: 'green',
  textColor: 'white',
  timeout: 1000,
  position: 'top',
});

export function dangerNotify(message) {
  Notify.create({
    type: 'danger-notif',
    message: message
  })
}

export function successNotify(message) {
  Notify.create({
    type: 'success-notif',
    message: message
  })
}
