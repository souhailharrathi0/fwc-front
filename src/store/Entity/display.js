import {
  IRRIGATION_TASKS_ENTITY_NAME,
  SENSOR_ENTITY_NAME,
  TREE_ENTITY_NAME,
  USER_ENTITY_NAME,
  WATER_SOURCE_ENTITY_NAME,
  WATER_SWITCHER_ENTITY_NAME,
  ZONE_ENTITY_NAME
} from "src/store/Entity/mutation-types";

export const DISPLAY = {
  [USER_ENTITY_NAME]: {
    name: "users",
    add_success: 'The user has been added successfully',
    update_success: 'The user has been updated successfully',
    delete_success: 'The user has been deleted successfully',
    delete_confirm: 'Do you really want to delete this user?',
  },
  [ZONE_ENTITY_NAME]: {
    name: "zones",
    add_success: 'The zone has been added successfully',
    update_success: 'The zone has been updated successfully',
    delete_success: 'The zone has been deleted successfully',
    delete_confirm: 'Do you really want to delete this zone?',
  },
  [IRRIGATION_TASKS_ENTITY_NAME]: {
    name: "Irrigation tasks",
    add_success: 'The irrigation task has been added successfully',
    update_success: 'The irrigation task has been updated successfully',
    delete_success: 'The irrigation task has been deleted successfully',
    delete_confirm: 'Do you really want to delete this irrigation task?',
  },
  [SENSOR_ENTITY_NAME]: {
    name: "sensors",
    add_success: 'The sensor has been added successfully',
    update_success: 'The sensor has been updated successfully',
    delete_success: 'The sensor has been deleted successfully',
    delete_confirm: 'Do you really want to delete this sensor?',
  },
  [TREE_ENTITY_NAME]: {
    name: "trees",
    add_success: 'The tree has been added successfully',
    update_success: 'The tree has been updated successfully',
    delete_success: 'The tree has been deleted successfully',
    delete_confirm: 'Do you really want to delete this tree?',
  },
  [WATER_SWITCHER_ENTITY_NAME]: {
    name: "water switchers",
    add_success: 'The water switcher has been added successfully',
    update_success: 'The water switcher has been updated successfully',
    delete_success: 'The water switcher has been deleted successfully',
    delete_confirm: 'Do you really want to delete this water switcher?',
  },
  [WATER_SOURCE_ENTITY_NAME]: {
    name: "water sources",
    add_success: 'The water source has been added successfully',
    update_success: 'The water source has been updated successfully',
    delete_success: 'The water source has been deleted successfully',
    delete_confirm: 'Do you really want to delete this water source?',
  }
}
