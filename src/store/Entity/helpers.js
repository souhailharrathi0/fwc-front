import {mapActions, mapGetters} from "vuex";
import {
  GET_ENTITY_LIST,
  GET_ENTITY_LIST_ALL,
  GET_ENTITY_SELECT_LIST,
  REMOVE_ENTITY_SELECT_LIST
} from "src/store/Entity/mutation-types";


export const EntityList = {
  ...mapGetters([
    "getEntityList",
    "getEntityListAll",
    "getEntitySelectList"
  ]),
};

export const CurrentEntity = {
  ...mapGetters([
    "getCurrentEntity",

  ]),
};

export const EntityListActions = {
  ...mapActions([
    GET_ENTITY_LIST,
    GET_ENTITY_LIST_ALL,
    GET_ENTITY_SELECT_LIST,
    REMOVE_ENTITY_SELECT_LIST
  ]),

};
