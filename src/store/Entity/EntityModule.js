import {
  ADD_ENTITY_TO_ENTITY_LIST,
  DELETE_ENTITY,
  GET_ENTITY,
  REMOVE_ENTITY_FROM_ENTITY_LIST,
  SAVE_ENTITY,
  SET_CURRENT_ENTITY,
  UPDATE_ENTITY_IN_ENTITY_LIST
} from "src/store/Entity/mutation-types";
import {responseErrorsHandler} from "src/plugins/ResponseErrorsHandler";
import {showMobileConfirmation, successNotify} from "src/plugins/Notify";
import ApiServices from "src/services/ApiServices";
import {DISPLAY} from "src/store/Entity/display";
import {Dialog, Platform} from "quasar";

/*
  * EntityStore is a store for a single entity by using the entity Name.
  * It is used to get, save and update a single entity by entity id.
*/
const entityModule = {
  state: {},
  getters: {
    getCurrentEntity: (state) => (entityName) => {
      return state[entityName + "CurrentEntity"] ? state[entityName + "CurrentEntity"] : null;
    }
  },
  mutations: {
    [SET_CURRENT_ENTITY](state, {entity, entityName}) {
      const entityNamePlural = entityName + 's';
      state[entityName + "CurrentEntity"] = entity;

    }
  },
  actions: {
    async [GET_ENTITY]({commit}, {entityId, entityName}) {
      try {
        const response = await ApiServices.getByEntityName(entityId, entityName);
        commit(SET_CURRENT_ENTITY, {entity: response.data, entityName: entityName});
        return response.data;
      } catch (e) {
        responseErrorsHandler(e);
        return null;
      }
    },
    async [SAVE_ENTITY]({state, commit, rootState}, {entityId = null, entity, entityName, showNotification = true}) {
      const entityNamePlural = entityName + 's';
      try {
        if (entityId) {
          const response = await ApiServices.patchByEntityName(entityId, entity, entityName);
          commit(SET_CURRENT_ENTITY, {entity: response.data, entityName: entityName});
          if (showNotification === true) {
            successNotify(DISPLAY[entityName].update_success);
          }
          if (rootState.entityListModule[entityNamePlural]) {
            commit(UPDATE_ENTITY_IN_ENTITY_LIST, {entity: response.data, entityName: entityName});
          }
          return response.data;
        }
        const response = await ApiServices.postByEntityName(entity, entityName);
        if (showNotification) {
          successNotify(DISPLAY[entityName].add_success);
        }
        if (rootState.entityListModule[entityNamePlural]) {
          commit(ADD_ENTITY_TO_ENTITY_LIST, {entity: response.data, entityName: entityName});
        }
        return response.data

      } catch (e) {
        responseErrorsHandler(e);
        return null;
      }
    },
    async [DELETE_ENTITY]({state, commit, rootState}, {entityId, entityName}) {
      const entityNamePlural = entityName + 's';
      try {
        //quasar delete request dialog
        //check is quasar is native or web

        // fot native app
        if (Platform.is.nativeMobile) {
          const value = await showMobileConfirmation(DISPLAY[entityName].delete_confirm)
          if (value) {
            await ApiServices.deleteByEntityName(entityId, entityName);
            successNotify(DISPLAY[entityName].delete_success);
            if (rootState.entityListModule[entityNamePlural]) {
              commit(REMOVE_ENTITY_FROM_ENTITY_LIST, {entityId: entityId, entityName: entityName});
            }
            return true;
          }
          return false;
        }

        Dialog.create({
          title: 'Confirmation',
          message: DISPLAY[entityName].delete_confirm,
          cancel: true,
        }).onOk(async () => {
            await ApiServices.deleteByEntityName(entityId, entityName);
            successNotify(DISPLAY[entityName].delete_success);
            if (rootState.entityListModule[entityNamePlural]) {
              commit(REMOVE_ENTITY_FROM_ENTITY_LIST, {entityId: entityId, entityName: entityName});
            }
            return true;
          }
        )
        return false;
      } catch (e) {
        responseErrorsHandler(e);
        return false;
      }
    }

  }
};

export default entityModule;
