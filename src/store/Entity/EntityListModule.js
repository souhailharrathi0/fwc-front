import {
  ADD_ENTITY_TO_ENTITY_LIST,
  GET_ENTITY_LIST,
  GET_ENTITY_LIST_ALL,
  GET_ENTITY_SELECT_LIST,
  REMOVE_ENTITY_FROM_ENTITY_LIST,
  REMOVE_ENTITY_SELECT_LIST,
  SET_ENTITY_LIST,
  SET_ENTITY_LIST_ALL,
  UPDATE_ENTITY_IN_ENTITY_LIST
} from "src/store/Entity/mutation-types";
import ApiServices from "src/services/ApiServices";
/**
 * @module EntityListModule
 * @type {{mutations: {[SET_ENTITY_LIST](*, {data: *, entityName: *}): void, [UPDATE_ENTITY_IN_ENTITY_LIST](*, {entity: *, entityName: *}): void, [ADD_ENTITY_TO_ENTITY_LIST](*, {entity: *, entityName: *}): void, [REMOVE_ENTITY_FROM_ENTITY_LIST](*, {entityId: *, entityName: *}): void}, state: {}, getters: {getEntityList: (function(*): function(*): {next: *, count: *, list: *|[]})}, actions: {[GET_ENTITY_LIST]({commit: *, state: *}, {filter?: null, entityName: *}): Promise<any|null|undefined>}}}
 In this module we use sdynamic module registration to create a state for each entity
 1- we use entityName to create the module name
 ex: entityName = "user" => state = {users: [], usersCount: 0, usersNext: null}

 */

const entityListModule = {
  state: {},
  getters: {
    getEntityList: (state) => (entityName) => {
      const entityNamePlural = entityName + 's';
      return {
        list: state[entityNamePlural] ? state[entityNamePlural] : [],
        count: state[entityNamePlural + 'Count'],
        next: state[entityNamePlural + 'Next'],
      };
    },
    getEntityListAll: (state) => (entityName) => {
      const entityNamePlural = entityName + 's';
      return state[entityNamePlural + 'All'] ? state[entityNamePlural + 'All'] : null;
    },
    getEntitySelectList: (state) => (entityName) => {
      let entitySelectList = state[entityName + 'SelectList'];
      return entitySelectList ? entitySelectList : null;
    },
  },
  mutations: {
    // Handler (state, data)
    //  The handler function is where we perform actual state modifications
    [SET_ENTITY_LIST](state, {data, entityName}) {
      /*
      * if data is not null we need to add the new data to the list
      * else we need to reset the list

       */
      const entityNamePlural = entityName + 's';
      if (data) {
        if (state[entityNamePlural]) {
          state[entityNamePlural] = state[entityNamePlural].concat(data.items);
        } else {
          state[entityNamePlural] = data.items;
        }
        state[entityNamePlural + 'Count'] = data.total;
        let next_page_number = data.page < data.pages ? data.page + 1 : null;
        state[entityNamePlural + 'Next'] = next_page_number !== null ? `/${entityNamePlural}/?page=${next_page_number}` : null;
        return
      }
      state[entityNamePlural] = [];
      state[entityNamePlural + 'Count'] = 0;
      state[entityNamePlural + 'Next'] = null;
    },
    [SET_ENTITY_LIST_ALL](state, {data, entityName}) {
      /*
      * if data is not null we need to add the new data to the list

       */
      const entityNamePlural = entityName + 's';
      if (data) {
        state[entityNamePlural + 'All'] = data;
        return
      }
      state[entityNamePlural + 'All'] = [];
    },
    [UPDATE_ENTITY_IN_ENTITY_LIST](state, {entity, entityName}) {
      const entityNamePlural = entityName + 's';
      if (state[entityNamePlural]) {
        const index = state[entityNamePlural].findIndex((item) => item.id === entity.id);
        if (index !== -1) {
          state[entityNamePlural][index] = entity;
        }

      }
    },
    [ADD_ENTITY_TO_ENTITY_LIST](state, {entity, entityName}) {
      const entityNamePlural = entityName + 's';
      if (state[entityNamePlural]) {
        //add entity to the beginning of the list
        state[entityNamePlural].unshift(entity);
        state[entityNamePlural + 'Count'] += 1;
      }
    },
    [REMOVE_ENTITY_FROM_ENTITY_LIST](state, {entityId, entityName}) {
      const entityNamePlural = entityName + 's';
      if (state[entityNamePlural]) {
        const index = state[entityNamePlural].findIndex((item) => item.id === entityId);
        if (index !== -1) {
          state[entityNamePlural].splice(index, 1);
          state[entityNamePlural + 'Count'] -= 1;
        }
      }
    },
    [GET_ENTITY_SELECT_LIST](state, {data, entityName}) {
      state[entityName + 'SelectList'] = data;
    },
    [REMOVE_ENTITY_SELECT_LIST](state, {entityName}) {
      state[entityName + 'SelectList'] = null;
    }
  },
  actions: {
    // get entity list with pagination
    async [GET_ENTITY_LIST]({commit, state}, {filter = null, entityName}) {
      const entityNamePlural = entityName + 's';

      if (filter) {
        /*
        * if filter is not null we need to reset the list
        ex: if we are searching for users with name = "john"
        * we need to reset the list before getting the new list
         */
        commit(SET_ENTITY_LIST, {data: null, entityName: entityName});
      }
      try {
        const response = await ApiServices.getListByEntityName(state[entityNamePlural + "Next"], filter, entityName);
        commit(SET_ENTITY_LIST, {data: response.data, entityName: entityName});
        return response.data;
      } catch (e) {
        console.log(e.response.data);
        return null;
      }
    },
    // get entity list without pagination
    async [GET_ENTITY_LIST_ALL]({commit, state}, {filter = null, entityName}) {

      try {
        const response = await ApiServices.getListAllByEntityName(filter, entityName);
        commit(SET_ENTITY_LIST_ALL, {data: response.data, entityName: entityName});

        return response.data;
      } catch (e) {
        console.log(e.response.data);
        return null;

      }
    },

    async [GET_ENTITY_SELECT_LIST]({commit, state}, {filter = null, entityName}) {

      try {
        const response = await ApiServices.getSelectListByEntityName(entityName, filter);
        commit(GET_ENTITY_SELECT_LIST, {data: response.data, entityName: entityName});

        return response.data;
      } catch (e) {
        console.log(e.response.data);
        return null;

      }
    },
    async [REMOVE_ENTITY_SELECT_LIST]({commit, state}, {entityName}) {
      commit(REMOVE_ENTITY_SELECT_LIST, {entityName: entityName});
    }
  }
};

export default entityListModule;
