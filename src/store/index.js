import {store} from 'quasar/wrappers'
import {createStore} from 'vuex'
import userModule from "src/apps/users/store/UserModule";
import entityListModule from "src/store/Entity/EntityListModule";
import entityModule from "src/store/Entity/EntityModule";
import zoneModule from "src/apps/zones/store/ZoneModule";
import sensorModule from "src/apps/sensors/store/SensorModule";


/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      userModule,
      entityListModule,
      entityModule,
      zoneModule,
      sensorModule
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING
  })

  return Store
})
