// TODO this file is the clientAPI to communicate with your API
import axios from "axios"; // contains the base url check in App.vue
import {formDataHeaders, jsonHeaders} from "src/constants/global";

function getEntityUrl(entityName) {
  return '/' + entityName + 's/';
}

export default {
  // *********** GETS ******************************
  // next it's an url, (next used for pagination)
getListByEntityName(next, filter, entityName) {
  let url = getEntityUrl(entityName);
  if (filter && !next) {
    // Encode the filters as a JSON string and then URL encode the string
    const filtersStr = encodeURIComponent(JSON.stringify(filter));
    url += '?filters=' + filtersStr;
  }
  console.log(url);
  if (next) {
    return axios.get(next);
  }
  return axios.get(url);
},

  // without pagination
  getListAllByEntityName(filter, entityName) {
    let url = getEntityUrl(entityName) + 'all/';
    if (filter) {
      url += '?';
      for (let key in filter) {
        url += key + '=' + filter[key] + '&';
      }
    }
    return axios.get(url);
  },
  getByEntityName(id, entityName) {
    let url = getEntityUrl(entityName) + id + "/";
    return axios.get(url);
  },
  getByEntityIdentifier(identifier, entityName) {
    let url = getEntityUrl(entityName) + "identifier/" + identifier + "/";
    return axios.get(url);
  },

  getSelectListByEntityName(entityName, filter) {
    let url = getEntityUrl(entityName) + 'select-list/';
    if (filter) {
      const queryParams = new URLSearchParams(filter).toString();
      url += '?filters=' + queryParams;
    }

    // Logic for populating the field from the backend
    return axios.get(url);
  },


  getStatsByEntityName(entityName, identifier) {
    let url = getEntityUrl(entityName) + 'stats/';
    if (identifier) {
      url += identifier + "/";
    }
    return axios.get(url);
  },

  // *********** PATCH / POST / Delete ********************

  patchByEntityName(id, data, entityName) {
    let url = getEntityUrl(entityName) + id + "/";

    if (data instanceof FormData) {
      return axios.patch(url, data, formDataHeaders);
    }

    return axios.patch(url, data);
  },
  postByEntityName(data, entityName) {
    let url = getEntityUrl(entityName);
    if (data instanceof FormData) {
      return axios.post(url, data, formDataHeaders);
    }
    return axios.post(url, data, jsonHeaders);
  },
  deleteByEntityName(id, entityName) {
    let url = getEntityUrl(entityName) + id + "/";
    return axios.delete(url);
  },

};
