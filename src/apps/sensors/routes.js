import SensorsPage from "src/apps/sensors/pages/SensorPage.vue";

const sensorsRoutes = [
  {
    path: '/sensors/:zone(\\d+)?',
    name: 'sensors',
    component: SensorsPage,
    props: route => ({zone: route.params.zone ? Number(route.params.zone) : null}),
    children: [
      {
        path: '',
        name: 'sensors',
        component: SensorsPage
      }
    ]

  },
]

export default sensorsRoutes
