import {mapGetters} from "vuex";


export const SensorTasks = {
  ...mapGetters([
    "getSensor",
  ]),
};
