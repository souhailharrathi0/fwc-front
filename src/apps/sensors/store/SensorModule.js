const sensorModule = {

  getters: {
    getFarm(state, _, rootState) {
      return rootState.entityListModule.sensors
    },
  },
};

export default sensorModule;

