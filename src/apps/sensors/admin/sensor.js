export const sensorColumns = [
  {
    name: 'serial_number',
    required: true,
    label: 'Serial Number',
    align: 'left',

  },

  {
    name: 'is_active',
    required: true,
    label: 'Status',
    align: 'left',

  },

  {
    name: 'type',
    required: true,
    label: 'Type',
    align: 'left',

  },

  {
    name: 'assigned_to',
    required: true,
    label: 'Assigned to',
    align: 'left',

  },

  {
    name: 'created_date',
    required: true,
    label: 'Created Date',
    align: 'left',

  },

  {
    name: 'updated_date',
    required: true,
    label: 'Updated Date',
    align: 'left',

  },

  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',

  },

  {
    name: 'actions',
    required: true,
    label: 'Actions',
    align: 'left',
  },

]
