import FormsValidationRules from "src/plugins/FormsValidationRules";
import {WATER_SOURCE_ENTITY_NAME, WATER_SWITCHER_ENTITY_NAME, ZONE_ENTITY_NAME} from "src/store/Entity/mutation-types";


export const SENSORS_TYPE_CHOICES = [
  {label: "Switcher Sensor", value: "switcher_sensor"},
  {label: "Source Sensor", value: "source_sensor"},
  {label: "Temperature Sensor", value: "temperature_sensor"},
  {label: "Moisture Sensor", value: "moisture_sensor"},
  {label: "Saturation Sensor", value: "saturation_sensor"},

]

export const ASSIGNED_TO_TYPE_CHOICES = [
  {label: "Zone", value: "zone"},
  {label: "Water Source", value: "water_source"},
  {label: "Water Switcher", value: "water_switcher"},

]

export const sensorFields = [
  {name: "serial_number", label: "Serial Number", type: "text", rules: [FormsValidationRules.requiredRules]},
  // Sensor Type
  {
    name: "type",
    label: "Type",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    // TODO this need to be fetch from the BackEND (check with Hamoud): Answer we can user an ENTITY select-list (check what he did in product)
    options: SENSORS_TYPE_CHOICES
  },
  {
    name: "assigned_to_type",
    label: "Assigned to",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    options: ASSIGNED_TO_TYPE_CHOICES
  },

  {
    name: "zone_name", label: "Zone", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME
  },
  {
    name: "water_source", label: "Water Source", type: "select",
    options: [],
    entityName: WATER_SOURCE_ENTITY_NAME
  },

    {
    name: "water_switcher", label: "Water Switcher", type: "select",
    options: [],
    entityName: WATER_SWITCHER_ENTITY_NAME
  },

  // Is Active
  {
    name: "is_active", label: "Is Active ?",
    type: "boolean", rules: [FormsValidationRules.requiredRules]
  },

]

export const sensorsFilterFields = [
  {
    name: "type",
    label: "Type",
    type: "select",
    options: SENSORS_TYPE_CHOICES,
  },
  {
    name: "zone_name", label: "Zone", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME,
  },


]
