import TreePage from "src/apps/tree/pages/TreePage.vue";

const treeRoutes = [
  {
    path: 'trees',
    name: 'tress',
    component: TreePage,
    children: [
      {
        path: '',
        name: 'trees',
        component: TreePage
      }
    ]

  },
]

export default treeRoutes
