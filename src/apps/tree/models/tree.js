import FormsValidationRules from "src/plugins/FormsValidationRules";

import {ZONE_ENTITY_NAME} from "src/store/Entity/mutation-types";

export const treeFields = [
  {name: "serial_number", label: "Serial Number", type: "text", rules: [FormsValidationRules.requiredRules]},
  {
    name: "zone_name", label: "Zone Name", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },
  {name: "variety", label: "Variety", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "height", label: "Height", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "width_diameter", label: "Width Diameter", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "trunk_diameter", label: "Trunk Diameter", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "planting_date", label: "Planting Date", type: "date", rules: [FormsValidationRules.requiredRules]},

]

export const treeFilterFields = [
  {
    name: "zone_name", label: "Zone Name", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME,
  },
  {name: "variety", label: "Variety", type: "text"},

]
