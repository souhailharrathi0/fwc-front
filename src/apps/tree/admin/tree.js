export const treeColumns = [
  {
    name: 'serial_number',
    required: false,
    label: 'Serial Number',
    align: 'left',

  },
  {
    name: 'zone_name',
    required: false,
    label: 'Zone',
    align: 'left',

  },
  {
    name: 'variety',
    required: true,
    label: 'Variety',
    align: 'left',

  },
  {
    name: 'height',
    required: true,
    label: 'Height',
    align: 'left',

  },
  {
    name: 'width_diameter',
    required: true,
    label: 'Width Diameter',
    align: 'left',

  },
  {
    name: 'trunk_diameter',
    required: true,
    label: 'Trunk Diameter',
    align: 'left',

  },
  {
    name: 'planting_date',
    required: true,
    label: 'Panning Date',
    align: 'left',

  },

  {
    name: 'updated_date',
    required: true,
    label: 'Updated Date',
    align: 'left',

  },

  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',

  },
  {
    name: 'actions',
    required: true,
    label: 'Actions',
    align: 'left',
  },

]
