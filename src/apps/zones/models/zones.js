import FormsValidationRules from "src/plugins/FormsValidationRules";

import {FARM_ENTITY_NAME} from "src/store/Entity/mutation-types";

export const zoneFields = [
  {name: "zone_name", label: "Name", type: "text", rules: [FormsValidationRules.requiredRules]},
  {
    name: "farm_name", label: "Farm", type: "select",
    options: [],
    entityName: FARM_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },
  {name: "size", label: "Size", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "soil_type", label: "Soil type", type: "text", rules: [FormsValidationRules.requiredRules]},
  {
    name: "moisture_trigger_start",
    label: "Moisture Trigger Start",
    type: Number,
    rules: [FormsValidationRules.requiredRules]
  },
  {
    name: "moisture_trigger_stop",
    label: "Moisture Trigger Stop",
    type: Number,
    rules: [FormsValidationRules.requiredRules]
  },
  {
    name: "saturation_trigger_start",
    label: "Saturation Trigger Start",
    type: Number,
    rules: [FormsValidationRules.requiredRules]
  },
  {
    name: "saturation_trigger_stop",
    label: "Saturation Trigger Stop",
    type: Number,
    rules: [FormsValidationRules.requiredRules]
  },

]

export const zoneFilterFields = [
    {name: "soil_type", label: "Soil type", type: "text"}
]
