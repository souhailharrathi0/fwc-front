import zonePage from "src/apps/zones/pages/ZonePage.vue";
import ZoneTable from "src/apps/zones/components/ZoneTable.vue";
import ZoneDetails from "src/apps/zones/components/ZoneDetails.vue";

const zonesRoutes = [
  {
    path: 'zones',
    name: 'zones-page',
    component: zonePage,
    children: [
      {
        path: '',
        name: 'zones',
        component: ZoneTable
      },
      {
        path: ':zoneId',
        component: ZoneDetails,
        name: 'zoneDetails', //#TODO SEIF !!! use the name always to call in router link
        props: (route) => {
          const zoneId = Number.parseInt(route.params.zoneId, 10);
          if (Number.isNaN(zoneId)) {
            return 0;
          }
          return {zoneId};
        }
      }
    ]

  },
]

export default zonesRoutes
