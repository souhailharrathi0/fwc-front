export const zoneColumns = [
  {
    name: 'zone_name',
    required: true,
    label: 'Name',
    align: 'left',

  },
  {
    name: 'farm_name',
    required: true,
    label: 'Farm',
    align: 'left',

  },
  {
    name: 'size',
    required: true,
    label: 'Surface area',
    align: 'left',

  },
  {
    name: 'soil_type',
    required: true,
    label: 'Soil type',
    align: 'left',

  },
  {
    name: 'moisture_trigger_start',
    required: true,
    label: 'Moisture Trigger Start',
    align: 'left',

  },
  {
    name: 'moisture_trigger_stop',
    required: true,
    label: 'Moisture Trigger Stop',
    align: 'left',

  },
  {
    name: 'saturation_trigger_start',
    required: true,
    label: 'Saturation Trigger Start',
    align: 'left',

  },
  {
    name: 'saturation_trigger_stop',
    required: true,
    label: 'Saturation Trigger Stop',
    align: 'left',
  },
  {
    name: 'created_date',
    required: true,
    label: 'Created Date',
    align: 'left',
  },
  {
    name: 'created_date',
    required: true,
    label: 'Created Date',
    align: 'left',
  },
  {
    name: 'updated_date',
    required: true,
    label: 'Updated Date',
    align: 'left',
  },
  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',
  },
  {
    name: 'actions',
    required: true,
    label: 'Actions',
    align: 'left',
  },
]

