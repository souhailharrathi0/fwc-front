const zoneModule = {

  getters: {
    getZones(state, _, rootState) {
      return rootState.entityListModule.zones
    },
  },
};

export default zoneModule;

