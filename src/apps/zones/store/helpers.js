import {mapGetters} from "vuex";


export const Zones = {
  ...mapGetters([
    "getZones",
  ]),
};
