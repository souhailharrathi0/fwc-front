import FarmPage from "src/apps/farm/pages/FarmPage.vue";

const farmRoutes = [
  {
    path: 'farm',
    name: 'farm',
    component: FarmPage,
    children: []

  },
]

export default farmRoutes
