import {mapGetters} from "vuex";


export const Farm = {
  ...mapGetters([
    "getFarm",
  ]),
};
