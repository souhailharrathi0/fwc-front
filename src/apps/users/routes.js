import UsersPage from "src/apps/users/pages/UsersPage.vue";
import UserTable from "src/apps/users/components/UserTable.vue";

const usersRoutes = [
  {
    path: 'users',
    name: 'users-page',
    component: UsersPage,
    children: [
      {
        path: '',
        name: 'users',
        component: UserTable
      }
    ]

  },
]

export default usersRoutes
