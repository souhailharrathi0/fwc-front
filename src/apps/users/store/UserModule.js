import AuthStore from "src/apps/users/store/AuthStore";

const userModule = {
  state: {},
  getters: {
    getUsers(state, _, rootState) {
      return rootState.entityListModule.users
    },
    // get user full name by id
    getUserFullNameById: (state, getters) => (id) => {
      const users = getters.getUsers;
      if (users) {
        const user = users.find((user) => user.id === id);
        if (user) {
          return `${user.first_name} ${user.last_name}`;
        }
      }
      return "loading..."
    }
  },
  mutations: {},
  actions: {},
  modules: {
    AuthStore,
  }
};

export default userModule;
