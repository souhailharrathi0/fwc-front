import {AUTHENTICATE_USER, LOAD_ESSENTIAL_DATA, LOGIN, LOGOUT, VERIFY_TOKEN} from "src/apps/users/mutation-types";
import AuthServices from "src/apps/users/services/AuthServices";
import axios from "axios";
import jwt_decode from "jwt-decode";
import {responseErrorsHandler} from "src/plugins/ResponseErrorsHandler";
import UsersServices from "src/apps/users/services/UsersServices";
import {Loading} from "quasar";
import {GET_ENTITY_LIST} from "src/store/Entity/mutation-types";

function setAxiosHeaders(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
}

function getUserIdFromToken(token) {
  return jwt_decode(token).user_id;
}


function clearLocalStorage() {
  localStorage.removeItem("access");

}

const AuthStore = {
  state: {
    currentUser: null,
    isAuthenticated: null,
  },
  getters: {
    getLoggedUser(state) {
      return state.currentUser;
    },
    getIsAuthenticated(state) {
      return state.isAuthenticated;
    },
  },
  mutations: {

    [AUTHENTICATE_USER](state, userData) {
      state.currentUser = userData;
      state.isAuthenticated = true;
    }
  },
  actions: {
    async [LOGIN]({dispatch}, {data}) {
      Loading.show();
      try {
        const response = await AuthServices.login(data);
        localStorage.setItem("access", JSON.stringify(response.data));
        await dispatch(AUTHENTICATE_USER, response.data.token);
        await dispatch(GET_ENTITY_LIST, {entityName: "user"})
        Loading.hide();
        return true;
      } catch (e) {

        responseErrorsHandler(e)
        console.log(e.response.data);
        Loading.hide();
        return false;
      }

    },
    async [LOGOUT]({}) {
      Loading.show();
      clearLocalStorage();
      this.$router.push({name: "login"});
      Loading.hide();
    },
    async [VERIFY_TOKEN]({dispatch}) {
      Loading.show();
      const access = localStorage.getItem("access");
      if (access) {
        const accessJson = JSON.parse(access);
        try {
          await AuthServices.verifyToken(accessJson.token)
          await dispatch(AUTHENTICATE_USER, accessJson.token);
          Loading.hide();

          return true;
        } catch (e) {
          console.log(e.response.data.detail);
          clearLocalStorage();
          Loading.hide();
          return false;
        }
      }
      Loading.hide();
      return null;


    },
    async [AUTHENTICATE_USER]({commit, dispatch}, token) {
      await setAxiosHeaders(token);
      const userId = getUserIdFromToken(token)
      try {
        const response = await UsersServices.getUserById(userId);
        await commit(AUTHENTICATE_USER, response.data);

      } catch (e) {
        responseErrorsHandler(e)
        clearLocalStorage();
        this.$router.push({name: "login"});
      }

    },
    async [LOAD_ESSENTIAL_DATA]({dispatch}) {
    }


  }
};

export default AuthStore;
