import {mapGetters} from "vuex";


export const CurrentUser = {
  ...mapGetters([
    "getLoggedUser",
    "getIsAuthenticated",

  ]),
};

export const Users = {
  ...mapGetters([

    "getUsers",
    "getUserFullNameById",
  ]),
};
