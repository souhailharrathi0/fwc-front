import FormsValidationRules from "src/plugins/FormsValidationRules";
import {FARM_ENTITY_NAME} from "src/store/Entity/mutation-types";

export const userFields = [
  {name: "last_name", label: "Last Name", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "first_name", label: "First Name", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "username", label: "Username", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "text_password", label: "Password", type: "password", rules: [FormsValidationRules.requiredRules]},
  {name: "email", label: "Email", type: "text", rules: [FormsValidationRules.emailRules]},
  {name: "phone_number", label: "Phone Number", type: "text", rules: [FormsValidationRules.phoneNumberRule]},

  // Role
  {
    name: "role",
    label: "Role",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    options: [
      {label: "Owner", value: "owner"},
      {label: "Admin", value: "admin"},
      {label: "Worker", value: "worker"},
      {label: "Engineer", value: "engineer"},

    ]
  },

  {
    name: "farm_name", label: "Farm", type: "select",
    options: [],
    entityName: FARM_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },

  // Is Active
  {
    name: "is_active", label: "Is Active ?",
    type: "boolean", rules: [FormsValidationRules.requiredRules]
  },
]

export const userFilterFields = [
  {name: "last_name", label: "Last Name", type: "text"},
  {name: "first_name", label: "First Name", type: "text"},


]
