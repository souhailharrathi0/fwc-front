export const LOGIN = "LOGIN";
export const LOGIN_BAR_CODE = "LOGIN_BAR_CODE";
export const LOGOUT = "LOGOUT";
export const VERIFY_TOKEN = "VERIFY_TOKEN";

export const AUTHENTICATE_USER = "AUTHENTICATE_USER";

export const LOAD_ESSENTIAL_DATA = "LOAD_ESSENTIAL_DATA";
