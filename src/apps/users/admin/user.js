export const userColumns = [
  {
    name: 'first_name',
    required: true,
    label: 'First Name',
    align: 'left',

  },

  {
    name: 'last_name',
    required: true,
    label: 'Last Name',
    align: 'left',

  },

  {
    name: 'username',
    required: true,
    label: 'Username',
    align: 'left',

  },

  {
    name: 'email',
    required: true,
    label: 'Email',
    align: 'left',

  },

  {
    name: 'phone_number',
    required: true,
    label: 'Phone Number',
    align: 'left',

  },

  {
    name: 'role',
    required: true,
    label: 'Role',
    align: 'left',

  },

  {
    name: 'is_active',
    required: true,
    label: 'is Active',
    align: 'left',

  },

  {
    name: 'farm_name',
    required: true,
    label: 'Farm',
    align: 'left',
  },

  {
    name: 'created_date',
    required: true,
    label: 'Created Date',
    align: 'left',

  },

  {
    name: 'updated_date',
    required: true,
    label: 'Updated Date',
    align: 'left',

  },

  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',

  },

  {
    name: 'actions',
    required: true,
    label: 'Actions',
    align: 'left',
  },
]
