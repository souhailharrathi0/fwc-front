import axios from "axios";

export default {
  login(data) {
    return axios.post("/auth/login/", data);
  },
  logout() {
    return axios.post("/auth/logout/");
  },
  // refreshToken() {
  //   return axios.post("/auth/token/refresh/");
  // },
  verifyToken(token) {
    return axios.post("/auth/token/verify/", {token: token}, {
      headers: {"Content-Type": "application/json"}
    });
  }
};
