import IrrigationTasksPage from "src/apps/irrigation-tasks/pages/IrrigationTasksPage.vue";
import IrrigationTasksTable from "src/apps/irrigation-tasks/components/IrrigationTasksTable.vue";

const irrigationTasksRoutes = [
  {
    path: 'irrigation-tasks',
    name: 'irrigation-tasks',
    component: IrrigationTasksPage,
    children: [
      {
        path: '',
        name: 'irrigation-tasks',
        component: IrrigationTasksTable
      }
    ]

  },
]

export default irrigationTasksRoutes
