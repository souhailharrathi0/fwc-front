import FormsValidationRules from "src/plugins/FormsValidationRules";

export const irrigationFields = [
  {
    name: "status",
    label: "Status",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    options: [
      {label: "Stop", value: "canceled"},
    ]
  },
]


export const STATUS_CHOICES = [
  {label: "Completed", value: "completed"},
  {label: "In progress", value: "in progress"},
  {label: "Pending", value: "pending"},
  {label: "Canceled", value: "canceled"},


]
export const irrigationFFilterFields = [
  {
    name: "status",
    label: "Status",
    type: "select",
    options: STATUS_CHOICES
  },

]
