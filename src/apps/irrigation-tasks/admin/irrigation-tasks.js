export const irrigationColumns = [
  {
    name: 'zone_name',
    required: true,
    label: 'Zone Name',
    align: 'left',

  },

  {
    name: 'status',
    required: true,
    label: 'Status',
    align: 'left',

  },

  {
    name: 'created_date',
    required: true,
    label: 'Start Time',
    align: 'left',

  },

  {
    name: 'updated_date',
    required: true,
    label: 'End Time',
    align: 'left',

  },

  {
    name: 'duration',
    required: true,
    label: 'Duration (min)',
    align: 'left',

  },

  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',

  },

]

