export const waterSwitcherColumns = [
  {
    name: 'serial_number',
    required: false,
    label: 'Serial Number',
    align: 'left',

  },

  {
    name: 'status',
    required: false,
    label: 'Status',
    align: 'left',

  },

  {
    name: 'sensor_serial_number',
    required: false,
    label: 'Sensor',
    align: 'left',

  },

  {
    name: 'water_source_serial_number',
    required: false,
    label: 'Water Source',
    align: 'left',

  },

  {
    name: 'zone_name',
    required: false,
    label: 'Zone',
    align: 'left',

  },

    {
    name: 'created_date',
    required: true,
    label: 'Created Date',
    align: 'left',

  },

  {
    name: 'updated_date',
    required: true,
    label: 'Updated Date',
    align: 'left',

  },

  {
    name: 'performed_by',
    required: true,
    label: 'Performed By',
    align: 'left',

  },

  {
    name: 'actions',
    required: true,
    label: 'Actions',
    align: 'left',
  },

]

