import FormsValidationRules from "src/plugins/FormsValidationRules";
import {SENSOR_ENTITY_NAME, WATER_SOURCE_ENTITY_NAME, ZONE_ENTITY_NAME} from "src/store/Entity/mutation-types";

export const waterSwitcherFields = [
  {name: "serial_number", label: "Serial Number", type: "text", rules: [FormsValidationRules.requiredRules]},
  {
    name: "sensor_serial_number", label: "Sensor", type: "select",
    options: [],
    entityName: SENSOR_ENTITY_NAME,
    filter: {
      "type__eq": "switcher_sensor",
      "is_active__eq": true
    },
  },
  {
    name: "zone_name", label: "Zone", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },
  {
    name: "water_source_serial_number", label: "Water Source", type: "select",
    options: [],
    entityName: WATER_SOURCE_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },
  // Status
   {
    name: "status",
    label: "Status",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    options: [
      {label: "On", value: "On"},
      {label: "Off", value: "Off"},

    ]
  },




]

export const waterSwitcherFilterFields = [
  {
    name: "zone_name", label: "Zone", type: "select",
    options: [],
    entityName: ZONE_ENTITY_NAME,
  },
]
