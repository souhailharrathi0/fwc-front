import WaterSwitcherPage from "src/apps/water_switcher/pages/WaterSwitcherPage.vue";

const waterSwitcherRoutes = [
  {
    path: 'water-switcher',
    name: 'water-switcher',
    component: WaterSwitcherPage,
    children: [
      {
        path: '',
        name: 'Water Switcher',
        component: WaterSwitcherPage
      }
    ]

  },
]

export default waterSwitcherRoutes
