import FormsValidationRules from "src/plugins/FormsValidationRules";

import {FARM_ENTITY_NAME, SENSOR_ENTITY_NAME} from "src/store/Entity/mutation-types";

export const waterSourceFields = [
  {name: "serial_number", label: "Serial Number", type: "text", rules: [FormsValidationRules.requiredRules]},
  {
    name: "sensor_serial_number", label: "Sensor", type: "select",
    options: [],
    entityName: SENSOR_ENTITY_NAME,
  },
  {
    name: "farm_name", label: "Farm", type: "select",
    options: [],
    entityName: FARM_ENTITY_NAME,
    rules: [FormsValidationRules.requiredRules]
  },
  {name: "type", label: "Type", type: "text", rules: [FormsValidationRules.requiredRules]},
  {name: "water_pressure", label: "Water Pressure", type: "number", rules: [FormsValidationRules.requiredRules]},
  {name: "water_volume", label: "Water Volume", type: "number", rules: [FormsValidationRules.requiredRules]},


  {
    name: "status",
    label: "Status",
    type: "select",
    rules: [FormsValidationRules.requiredRules],
    options: [
      {label: "On", value: "On"},
      {label: "Off", value: "Off"},

    ]
  },

]
