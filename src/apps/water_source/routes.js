import WaterSourcePage from "src/apps/water_source/pages/WaterSourcePage.vue";

const waterSourceRoutes = [
  {
    path: 'water-source',
    name: 'water-source',
    component: WaterSourcePage,
    children: [
      {
        path: '',
        name: 'Water Source',
        component: WaterSourcePage
      }
    ]

  },
]

export default waterSourceRoutes
