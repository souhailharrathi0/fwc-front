import usersRoutes from "src/apps/users/routes";
import zonesRoutes from "src/apps/zones/routes";
import irrigationTasksRoutes from "src/apps/irrigation-tasks/routes";
import farmRoutes from "src/apps/farm/routes";
import sensorsRoutes from "src/apps/sensors/routes";
import treeRoutes from "src/apps/tree/routes";
import waterSwitcherRoutes from "src/apps/water_switcher/routes";
import waterSourceRoutes from "src/apps/water_source/routes";

const childrenRoutes = [{path: 'home', component: () => import('pages/IndexPage.vue')}];
childrenRoutes.push.apply(childrenRoutes, usersRoutes);
childrenRoutes.push.apply(childrenRoutes, zonesRoutes);
childrenRoutes.push.apply(childrenRoutes, irrigationTasksRoutes);
childrenRoutes.push.apply(childrenRoutes, farmRoutes);
childrenRoutes.push.apply(childrenRoutes, sensorsRoutes);
childrenRoutes.push.apply(childrenRoutes, treeRoutes);
childrenRoutes.push.apply(childrenRoutes, waterSwitcherRoutes);
childrenRoutes.push.apply(childrenRoutes, waterSourceRoutes);
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: childrenRoutes,
    meta: {
      loginRequired: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('pages/LoginPage.vue'),
    meta: {
      disableIfLoggedIn: true,
    }
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
