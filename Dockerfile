# Use the official Node.js image as the base image
FROM node:14

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install project dependencies
RUN npm install

#install quasar
RUN npm install -g @quasar/cli
# Copy the rest of the application code to the container
COPY . .

# Build your Quasar project
RUN quasar build

# Command to start your Quasar app
CMD ["quasar", "dev"]
